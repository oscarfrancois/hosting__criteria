# Le choix d'un hébergeur

![cloud](img/cloud__private_vs_public.jpg)

# Introduction

Ce document décrit les critères à considérer pour le choix d'un hébergeur.
Il n'a pas pour vocation à être exhaustif, la liste des critères dépendant fortement de l'usage souhaité.

# Public cible

On supposera dans ce document que le lecteur est aguéri aux termes informatiques usuels dans le domaine de l'hébergement (ex: cloud, hybrid-cloud, self-hosting, SAAS, IAAS, PAAS, hyperviseur, conteneur, serverless, noops, etc).

# Les critères environnementaux

Une première question à se poser en amont: ais-je réellement besoin d'héberger à l'extérieur et en permanence mes données et services?

Par exemple pour un service groupé de calendrier/tâches/contact/données, une synchronisation une fois par jour chez soi peut être suffisante dans certains cas.

Chaque donnée dont l'hébergement est délégué a un impact environnemental. Que ce soit en consommation électrique pure, en besoin d'équipements de climatisation, en besoins matériels mais aussi humains.

# Les différents types d'hébergement

On peut distinguer en premier lieu les niveaux de déléguation de l'hébergement:

- hébergement sur site, auto-hébergement (on-premise hosting)
- hébergement hors site (off-site hosting)
- hébergement hybride (hybrid cloud)

On peut ensuite distinguer les types d'hébergement:

- serveur dédié (dedicated server)
- serveur partagé tel que machines virtuelles (VM) et conteneur (container)
- nuage classique (GCP, AWS, Azure)
- nuage hybride (hybrid-cloud)

# Les poids lourd du nuage (cloud) et des centres de données (datacenter)

- Google cloud platform (gcp).
- Amazon web services (aws).
- Microsoft Azure.
- IBM Openstack.

# Les acteurs de taille intermédiaires et historiques

La liste suivante contient quelques hébergeurs historiques ainsi que des hébergeurs ayant de bons rapports qualité prix pour les segments des PME/TPE.

- digitalocean.com : services de qualité et prix d'entrée attractifs (5$/mois, 1GB, 1CPU, 25GB SSD, 1TB)
- hébergeur historique Suisse: infomaniak
- hébergeur historique France: ovh
- hébergeurs contribuant au domaine public et associatif: bytemark.co.uk / gandi.net: hébergeurs historiques du web bien implémentés. Ils hébergent gracieusement des projets associatifs et de logiciel libre (openstreetmap, debian, vlc, eff, gnome, etc).

# Législation

Les critères liés à la juridiction qui s'applique sur l'emplacement du centre de données (datacenter) hébergeant le système d'information peuvent avoir une importance prioritaire.

[Voici à ce sujet le cas de la Suisse](https://www.rts.ch/play/tv/la-suisse-sous-couverture/video/la-suisse-sous-couverture-le-secret-bunker-suisse-45?id=10868236)

# Critères de sécurité

Les critères de sécurité varient selon la classification des risques considérés.

La classification des risques consiste à lister les menaces possibles et déterminer pour chacune leur probabilité d'occurrence ainsi que leur conséquence.
Ainsi un risque peu probable mais ayant des conséquences catastrophiques pour le système sera adressé prioritairement.
A contrario, une menace très probable mais n'ayant peu ou pas d'incidence sur le système aura une priorité faible.

# Illustration des problèmatique de choix d'hébergeur

Le cas du Guardian

[guardian](https://www.theguardian.com/info/2018/nov/30/bye-bye-mongo-hello-postgres)

